import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const Loader = ({type, message}) => {
  return (
    <div className="loader">
      <div className={`loader__${type}`}></div>
      <p>{message}</p>
    </div>
  )
}

Loader.defaultProps = {
  message: 'Loading',
  type: 'circle'
}

Loader.propTypes = {
  message: PropTypes.string,
  type: PropTypes.oneOf(['circle'])//add other loades types later
}

export default Loader;