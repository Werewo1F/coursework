import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './style.css';

class Tabs extends Component {

  state = {
    active: 0
  }

  handlerChange = (e) => {
    this.setState({active: parseInt(e.target.dataset.index)})
  }

  render() {
    const {active} = this.state;
    const {children} = this.props;
    const {handlerChange} = this;
    return (
      <>
      <ul className="nav nav-tabs">
        {
          React.Children.map(children, (el, i) => {
            let {children} = el.props;
            let isActive = (i === active) ? 'active' : null;
            this.outputTab = isActive ? children : this.outputTab;
            return React.cloneElement(
              el,
              {
                ...el.props,
                isActive: isActive,
                index: i,
                handler: handlerChange
              }
            )
          })
        }
      </ul>
      <div className="tab-content">
        <div className="tab-pane fade show active">{this.outputTab}</div>
      </div>
      </>
    )
  }
}

Tabs.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ]).isRequired
}

export default Tabs;