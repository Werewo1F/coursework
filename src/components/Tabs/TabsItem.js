import React from 'react';

const TabsItem = ({title, isActive, handler, index}) => {
  return (
    <li className="nav-item">
      <span data-index={index} onClick={handler} className={`nav-link ${isActive}`}>{title}</span>
    </li>
  )
}

export default TabsItem;