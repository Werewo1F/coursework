import React, {Component} from 'react';

//Rewrite later (separate style for diff blocks)
//Mb remove bootstrap later
import 'bootstrap/dist/css/bootstrap.css';

import Breadcrumbs from './Breadcrumbs';
import Button from './Button';
import Loader from './Loader';
import Confirm from './Confirm';
import Tabs from './Tabs';
import TabsItem from './Tabs/TabsItem';
// import Slider from './Slider';
// import Slide from './Slider/Slide';
import Card from './Card';
import Table from './Table';
import TableRow from './Table/TableRow';
import TableCell from './Table/TableCell';
import Sidebar from './Sidebar';
import SidebarItem from './Sidebar/SidebarItem';


class Main extends Component {

  click = () => {
    console.log('Click');
  }

  render() {
    const {click} = this;
    return (
      <main className="container-fluid">
        <div className="row justify-content-between align-items-stretch">
          <Sidebar>
            <SidebarItem title="Header" heading={true} />
            <SidebarItem title="Header with link"  link="google.com" heading={true} />
            <SidebarItem title="text" />
            <SidebarItem title="link" link="google.com" />
          </Sidebar>
          <div className="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <div className="row justify-content-between align-items-stretch">
              <Card size='large'>
                <Breadcrumbs
                  array={[
                    ['Home', '/'],
                    ['Custom', '/'],
                    ['current']
                  ]}
                />
              </Card>
              <Card size='small'>
                <Tabs>
                  <TabsItem title="Lorem Ipsum">
                    <h2>What is Lorem Ipsum?</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
                  </TabsItem>


                  <TabsItem title="Confirm Button">
                    <Confirm>
                      <Button handler={click} type='primary'>text</Button>
                    </Confirm>
                  </TabsItem>


                  <TabsItem title="Loader">
                    <Loader />
                  </TabsItem>


                  {/*<TabsItem title="Slider">
            <Slider>
              <Slide>
                <p>lol</p>
              </Slide>
              <Slide>
                <p>lol2</p>
              </Slide>
            </Slider>
          </TabsItem>*/}
                </Tabs>
              </Card>
              <Card size='small'>
                <h2>Table</h2>
                <Table>
                  <TableRow head={true}>
                    <TableCell>lol</TableCell>
                    <TableCell column={2}>lol</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell head={true}>lol</TableCell>
                    <TableCell>lol</TableCell>
                    <TableCell>lol</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>lol</TableCell>
                    <TableCell>lol</TableCell>
                    <TableCell>lol</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell column={2}> </TableCell>
                    <TableCell>lol</TableCell>
                  </TableRow>
                </Table>
              </Card>
              <Card size='small'>
                <Breadcrumbs
                  array={[
                    ['Home', '/'],
                    ['Custom', '/'],
                    ['current']
                  ]}
                />
              </Card>
            </div>
          </div>
        </div>

      </main>
    )
  }
}

export default Main;
