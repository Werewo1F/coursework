import React from 'react';

import './style.css';
import Button from '../Button';

const Popup = ({title, message, close, closeMessage, success}) => {
  return (
    <>
    <div className="modal fade show">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">{title}</h5>
          </div>
          <div className="modal-body">
            <p>{message}</p>
          </div>
          <div className="modal-footer">
            {success}
            <Button handler={close} type='secondary'>{closeMessage}</Button>
          </div>
        </div>
      </div>
    </div>
    <div className="modal-backdrop fade show"></div>
    </>
  )
}

export default Popup;