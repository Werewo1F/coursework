import React, {Component} from 'react';
import Button from '../Button';

class Slider extends Component {

  render() {
    const {children} = this.props;
    return (
      <div className="slider">
        <div className="btn-group">
          <Button type="secondary" size="small" icon="chevron-left"></Button>
          <Button type="secondary" size="small" icon="chevron-right"></Button>
        </div>
        <div className="viewport">
          {children}
        </div>
      </div>
    )
  }
}

export default Slider;