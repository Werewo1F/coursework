import React from 'react';

const Slide = ({children}) => {
  return (
    <div className='slider__item'>
      {children}
    </div>
  )
}

export default Slide;