import React from 'react';
import PropTypes from 'prop-types';

const Card = ({children, size}) => {
  let cardClass;
  switch (size) {
    case 'small':
      cardClass = 'card-wrapper col-sm-12 col-md-6 col-lg-4';
      break;
    case 'medium': {
      cardClass = 'card-wrapper col-md-12 col-lg-6';
      break;
    }
    default:
      cardClass = 'card-wrapper col-12';
  }
  return (
    <div className={cardClass}>
      <div className="card">
        {children}
      </div>
    </div>
  )
}

Card.defaultProps = {
  size: 'large'
}

Card.propTypes = {
  size: PropTypes.arrayOf(['small', 'medium', 'large'])
}

export default Card;