import React, {Component} from 'react';
import PropTypes from 'prop-types';

class TableRow extends Component {
  render() {
    let {children, head} = this.props;
    return (
      <tr>
        {
          React.Children.map(
            children,
            el => {
              return React.cloneElement(
                el,
                {
                  ...el.props,
                  head: el.props.head ? el.props.head : head
                }
              )
            }
          )
        }
      </tr>
    )
  }
}

TableRow.defaultProps = {
  head: false
}

TableRow.propTypes = {
  head: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ]).isRequired
}

export default TableRow;