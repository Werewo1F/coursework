import React from 'react';
import PropTypes from 'prop-types';

const Table = ({children}) => {
  return (
    <table className="table table-striped">
      <tbody>
        {children}
      </tbody>
    </table>
  )
}

Table.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ]).isRequired
}

export default Table;