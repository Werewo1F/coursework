import React from 'react';
import PropTypes from 'prop-types';

const TableCell = ({children, head, column}) => {
  return (
    <>
    {
      (head) ? (
        <th colspan={column}>{children}</th>
      ) : (
        <td colspan={column}>{children}</td>
      )
    }
    </>
  )
}

TableCell.defaultProps = {
  head: false,
  column: 1
}

TableCell.propTypes = {
  head: PropTypes.bool,
  column: PropTypes.number,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ]).isRequired
}

export default TableCell;