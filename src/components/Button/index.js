import React from 'react';
import PropTypes from 'prop-types';


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import library from '../fa-library';

const Button = ({children, type, status, size, icon, handler}) => {
  let sizeClass;
  switch (size) {
    case ('small') : {
      sizeClass = 'btn-sm';
      break;
    }
    case ('large') : {
      sizeClass = 'btn-lg';
      break;
    }
    default:
      sizeClass = '';
  }
  let attr = {
    className:`btn btn-${type} ${sizeClass}`,
    disabled: status === 'disabled' && 'disabled',
    onClick: handler
  }
  return (
    <button {...attr}>
      {children}
      {
        (icon) && (
          <FontAwesomeIcon  icon={icon} />
        )
      }
    </button>
  )
}

Button.defaultProps = {
  status: 'active',
  type: 'default',
  size: 'medium'
}

Button.propTypes = {
  icon: PropTypes.oneOf(Object.keys(library.definitions.fas)),
  status: PropTypes.oneOf(['active', 'disabled']),
  type: PropTypes.oneOf(['default', 'primary', 'secondary', 'link']).isRequired,
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  handler: PropTypes.func
}

export default Button;