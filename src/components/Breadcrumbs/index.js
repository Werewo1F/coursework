import React from 'react';
import PropTypes from 'prop-types';

const Breadcrumbs = ({array}) => {
  return (
    <nav aria-label="breadcrumb">
      <ol className="breadcrumb">
        {
          array.map((el, key) => {
            //Use scheme where 0 => name, 1 => link
            let brdClass = (array.length - 1 > key) ?
              "breadcrumb-item" :
              "breadcrumb-item active";
            return (
              <li key={key} className={brdClass}>
                {
                  (el[1]) ? (
                    //mb I must use Link but I not use route right now
                    <a href={el[1]}>
                      {el[0]}
                    </a>
                  ) : (
                    <span>
                    {el[0]}
                  </span>
                  )
                }
              </li>
            )
          })
        }
      </ol>
    </nav>
  )
}

Breadcrumbs.propTypes = {
  array: PropTypes.array.isRequired
}

export default Breadcrumbs;