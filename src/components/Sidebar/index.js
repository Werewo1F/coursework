import React from 'react';
import PropTypes from "prop-types";


const Sidebar = ({children}) => {
  return (
    <nav className="col-md-2 d-none d-md-block bg-light sidebar">
      <div className="sidebar-sticky">
        <ul className="nav flex-column">
          {children}
        </ul>
      </div>
    </nav>
  )
}


Sidebar.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ]).isRequired
}

export default Sidebar;