import React from 'react';
import PropTypes from 'prop-types';

const SidebarItem = ({title, link, heading}) => {
  return (
    <li className="nav-item">
      {
        (heading) ? (
          <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1">
            {
            (link.length > 0) ?
              <a href={link}>{title}</a> :
              <span>title</span>
            }
          </h6>
        ) : (
          <>
            {
              (link.length > 0) ?
                <a className="nav-link" href={link}>{title}</a> :
                <span className="nav-link">title</span>
            }
          </>
        )
      }
    </li>
  )
}

SidebarItem.defaultProps = {
  heading: false,
  link: ''
}

SidebarItem.propTypes = {
  title: PropTypes.string.isRequired,
  link: PropTypes.string,
  heading: PropTypes.bool
}

export default SidebarItem;