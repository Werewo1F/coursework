import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Popup from '../Popup';

class Confirm extends Component {
  state = {
    showPopup: false
  }

  handlerConfirm = () => {
    this.setState({showPopup: true});
  }

  handlerClose = () => {
    this.setState({showPopup: false});
  }

  render() {
    const {showPopup} = this.state;
    const {children} = this.props;
    const {handlerConfirm, handlerClose} = this;
    React.Children.only(children);
    return (
      <>
      {
        React.cloneElement(
          children,
          {
            ...children.props,
            handler: handlerConfirm,
          }
        )
      }
      {
        (showPopup) && (
          <Popup {...this.props} close={handlerClose} success={children} />
        )
      }
      </>
    )
  }
}

Confirm.defaultProps = {
  title: 'Warning!',
  message: 'Are you sure?',
  closeMessage: 'Back'
}

Confirm.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  closeMessage: PropTypes.string,
  children: PropTypes.element.isRequired
}



export default Confirm;